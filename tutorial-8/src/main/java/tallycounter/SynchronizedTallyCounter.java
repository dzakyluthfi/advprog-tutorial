package tallycounter;

/**
 * Created by LENOVO on 4/13/2018.
 */
public class SynchronizedTallyCounter {
    private int counter = 0;

    public synchronized void increment() {
        counter++;
    }

    public synchronized void decrement() {
        counter--;
    }

    public synchronized int value() {
        return counter;
    }

}

