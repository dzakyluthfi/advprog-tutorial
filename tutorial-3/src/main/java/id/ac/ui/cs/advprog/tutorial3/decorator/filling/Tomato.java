package id.ac.ui.cs.advprog.tutorial3.decorator.filling;

import id.ac.ui.cs.advprog.tutorial3.decorator.Food;

/**
 * Created by LENOVO on 3/2/2018.
 */
public class Tomato extends Food{
    Food food;

    public Tomato(Food food) {
        this.food = food;
        description = "adding tomato";
    }

    @Override
    public String getDescription() {
        return food.getDescription() + ", " + description;
    }

    @Override
    public double cost() {
        return food.cost() + 0.5;
    }
}
