package id.ac.ui.cs.advprog.tutorial3.decorator.filling;

import id.ac.ui.cs.advprog.tutorial3.decorator.Food;

/**
 * Created by LENOVO on 3/2/2018.
 */
public class Lettuce extends Food{
    Food food;

    public Lettuce(Food food) {
        this.food = food;
        description = "adding lettuce";
    }

    @Override
    public String getDescription() {
        return food.getDescription() + ", " + description;
    }

    @Override
    public double cost() {
        return food.cost() + 0.75;
    }
}
