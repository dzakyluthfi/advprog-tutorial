package id.ac.ui.cs.advprog.tutorial3.composite.techexpert;

import id.ac.ui.cs.advprog.tutorial3.composite.Employees;

/**
 * Created by LENOVO on 3/2/2018.
 */
public class FrontendProgrammer extends Employees {
    public FrontendProgrammer(String name, double salary) {
        if (salary >= 30000.00) {
            this.name = name;
            this.salary = salary;
            this.role = "Front End Programmer";
        } else{
            throw new IllegalArgumentException();
        }
    }

    @Override
    public double getSalary() {
        return salary;
    }
}
