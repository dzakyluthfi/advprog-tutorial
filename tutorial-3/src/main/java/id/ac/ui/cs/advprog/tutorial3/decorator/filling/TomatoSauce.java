package id.ac.ui.cs.advprog.tutorial3.decorator.filling;

import id.ac.ui.cs.advprog.tutorial3.decorator.Food;

/**
 * Created by LENOVO on 3/2/2018.
 */
public class TomatoSauce extends Food{
    Food food;

    public TomatoSauce(Food food) {
        this.food = food;
        description = "adding tomato sauce";
    }

    @Override
    public String getDescription() {
        return food.getDescription() + ", " + description;
    }

    @Override
    public double cost() {
        return food.cost() + 0.2;
    }
}
