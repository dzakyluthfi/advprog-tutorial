package id.ac.ui.cs.advprog.tutorial1.strategy;

public class Squeak {
    void quack() {
        System.out.println("Squeak");
    }
}
